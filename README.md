**LOCALIZACIÓN VENEZOLANA**

Scripts DDL y DML de la División Político Territorial de Venezuela, según los datos del Instituto Nacional de Estadística.

**REFERERENCIAS**

1. Instituto Nacional de Estadística de Venezuela: http://www.ine.gov.ve/

**CONTACTO**

Si quieres apoyar esta iniciativa puedes realizar una donación a esta cuenta [Paypal](https://www.paypal.me/HidalgoF)

Si quieres participar en el repositorio, escríbeme al correo de fhidalgo.dev@gmail.com