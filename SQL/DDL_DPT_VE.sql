-- Sábado 20 jun 2020
-- Model: DDL de la División Político Territorial Venezolana (DPT)
-- Version: 1.0
-- Autor: Franyer Hidalgo VE - @fhidalgo.dev

-- -----------------------------------------------------
-- Table `estados`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estados` (
  `id` INT(10) NOT NULL AUTO_INCREMENT,
  `codigo` VARCHAR(10) NOT NULL,
  `denominacion` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC));

-- -----------------------------------------------------
-- Table `municipios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `municipios` (
  `id` INT(10) NOT NULL AUTO_INCREMENT,
  `codigo` VARCHAR(10) NOT NULL,
  `denominacion` VARCHAR(100) NOT NULL,
  `estado_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_municipios_estados_idx` (`estado_id` ASC),
  CONSTRAINT `fk_municipios_estados`
    FOREIGN KEY (`estado_id`)
    REFERENCES `estados` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

-- -----------------------------------------------------
-- Table `parroquias`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `parroquias` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `codigo` VARCHAR(10) NOT NULL,
  `denominacion` VARCHAR(100) NOT NULL,
  `municipio_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_parroquias_municipios_idx` (`municipio_id` ASC),
  CONSTRAINT `fk_parroquias_municipios`
    FOREIGN KEY (`municipio_id`)
    REFERENCES `municipios` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
