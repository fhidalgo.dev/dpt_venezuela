-- Sábado 20 jun 2020
-- Model: DML de los Estados de la División Político Territorial Venezolana (DPT) según datos del INE
-- Version: 1.0
-- Autor: Franyer Hidalgo VE - @fhidalgo.dev

-- -----------------------------------------------------
-- Carga de los 25 Estados Venezolanos
-- -----------------------------------------------------

INSERT INTO estados(id, codigo, denominacion) VALUES ('1', 'AMA', 'Amazonas');
INSERT INTO estados(id, codigo, denominacion) VALUES ('2', 'ANZ', 'Anzoátegui');
INSERT INTO estados(id, codigo, denominacion) VALUES ('3', 'APU', 'Apure');
INSERT INTO estados(id, codigo, denominacion) VALUES ('4', 'ARA', 'Aragua');
INSERT INTO estados(id, codigo, denominacion) VALUES ('5', 'BAR', 'Barinas');
INSERT INTO estados(id, codigo, denominacion) VALUES ('6', 'BOL', 'Bolívar');
INSERT INTO estados(id, codigo, denominacion) VALUES ('7', 'CAR', 'Carabobo');
INSERT INTO estados(id, codigo, denominacion) VALUES ('8', 'COJ', 'Cojedes');
INSERT INTO estados(id, codigo, denominacion) VALUES ('9', 'DEA', 'Delta Amacuro');
INSERT INTO estados(id, codigo, denominacion) VALUES ('10', 'DTC', 'Distrito Capital');
INSERT INTO estados(id, codigo, denominacion) VALUES ('11', 'FAL', 'Falcón');
INSERT INTO estados(id, codigo, denominacion) VALUES ('12', 'GUA', 'Guárico');
INSERT INTO estados(id, codigo, denominacion) VALUES ('13', 'LAR', 'Lara');
INSERT INTO estados(id, codigo, denominacion) VALUES ('14', 'MER', 'Mérida');
INSERT INTO estados(id, codigo, denominacion) VALUES ('15', 'MIR', 'Miranda');
INSERT INTO estados(id, codigo, denominacion) VALUES ('16', 'MON', 'Monagas');
INSERT INTO estados(id, codigo, denominacion) VALUES ('17', 'NVE', 'Nueva Esparta');
INSERT INTO estados(id, codigo, denominacion) VALUES ('18', 'POR', 'Portuguesa');
INSERT INTO estados(id, codigo, denominacion) VALUES ('19', 'SUC', 'Sucre');
INSERT INTO estados(id, codigo, denominacion) VALUES ('20', 'TAC', 'Táchira');
INSERT INTO estados(id, codigo, denominacion) VALUES ('21', 'TRU', 'Trujillo');
INSERT INTO estados(id, codigo, denominacion) VALUES ('22', 'VAR', 'Vargas');
INSERT INTO estados(id, codigo, denominacion) VALUES ('23', 'YAR', 'Yaracuy');
INSERT INTO estados(id, codigo, denominacion) VALUES ('24', 'ZUL', 'Zulia');
INSERT INTO estados(id, codigo, denominacion) VALUES ('25', 'DPF', 'Dependencias Federales');
